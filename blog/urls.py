from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.post_list, {'published':True}, name='post_list'),
    url(r'^post/unpublished/$', views.post_list, {'published':False},name='w_post_list'),
    url(r'^post/(?P<pk>[0-9]+)/publish/$', views.post_publish, name='post_publish'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^post/edit/(?P<pk>[0-9]+)/$', views.post_edit, name='post_edit'),
    url(r'^post/delete/(?P<pk>[0-9]+)/$', views.post_delete, name='post_delete'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^comments/unapproved/$', views.unapproved_comments, name='unapproved_comments'),
    url(r'^comment/(?P<pk>[0-9]+)/approve/$', views.comment_approve, name='comment_approve'),
    url(r'^comment/(?P<pk>[0-9]+)/remove/$', views.comment_remove, name='comment_remove'),
]